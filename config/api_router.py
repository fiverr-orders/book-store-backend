from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from books.views import BookViewSet, BookChapterViewSet

router = DefaultRouter()

router.register(r"books", BookViewSet)
chapters_router = NestedDefaultRouter(router, r'books', lookup='book')
chapters_router.register(r'chapters', BookChapterViewSet)

app_name = "api"
urlpatterns = router.urls
