from django.db import models
from django.db.models import DO_NOTHING

from books.managers import BookManager


class Book(models.Model):
    class Meta:
        db_table = 'books'

    objects = BookManager()

    id = models.AutoField(primary_key=True)
    title = models.TextField()
    language = models.CharField(max_length=200)
    source = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    number_of_pages = models.IntegerField()
    published_date = models.IntegerField()
    published_location = models.CharField(max_length=200)
    publisher = models.CharField(max_length=200)
    publication_info = models.CharField(max_length=200)
    print_source = models.TextField()
    right_permissions = models.TextField()
    subject_terms = models.TextField()


class BookDetail(models.Model):
    class Meta:
        db_table = 'book_details'

    id = models.AutoField(primary_key=True)
    book = models.ForeignKey(Book, on_delete=DO_NOTHING, related_name="pages", null=True, blank=True)
    page_number = models.IntegerField()
    section_number = models.IntegerField()
    chapter_title = models.CharField(max_length=500)
    physical_description = models.TextField()
