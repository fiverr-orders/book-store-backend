from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from books.filters import BookSearchFilterBackend
from books.models import Book, BookDetail
from books.serializers import BookSerializer, BookDetailSerializer, BookChapterSerializer


@permission_classes((AllowAny,))
class BookViewSet(ReadOnlyModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = (BookSearchFilterBackend,)

    def retrieve(self, request, *args, **kwargs):
        book_id = int(kwargs.get('pk'))
        page_number = int(self.request.query_params.get('page_number', 1))
        book_detail = BookDetail.objects.filter(
            book=book_id,
            page_number=page_number
        ).first()

        if not book_detail:
            book_detail = BookDetail()
            book_detail.book = self.get_queryset().get(id=book_id)
            book_detail.page_number = page_number

        return Response(
            BookDetailSerializer(instance=book_detail).data
        )


@permission_classes((AllowAny,))
class BookChapterViewSet(ReadOnlyModelViewSet):
    queryset = BookDetail.objects.all()
    serializer_class = BookDetailSerializer

    def list(self, request, *args, **kwargs):
        book_id = self.kwargs.get('book_pk', None)
        queryset = BookDetail.objects.filter(book_id=book_id).order_by('page_number')

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
