from typing import Union

from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.db import models


def get_search_vectors(
    source=None, author=None, publisher=None, published_date=None, published_location=None, publication_info=None
):
    search_vectors = SearchVector('title', weight='A')

    if source:
        search_vectors += SearchVector('source')
    if author:
        search_vectors += SearchVector('author', weight='B')
    if publisher:
        search_vectors += SearchVector('publisher', weight='C')
    if published_date:
        search_vectors += SearchVector('published_date')
    if published_location:
        search_vectors += SearchVector('published_location')
    if publication_info:
        search_vectors += SearchVector('publication_info')

    return search_vectors


class BookManager(models.Manager):
    def search(
        self, text, source=None, author=None, publisher=None,
        published_date=None, published_location=None, publication_info=None
    ):
        search_vectors = get_search_vectors(
            source=source, author=author, publisher=publisher, published_date=published_date,
            published_location=published_location, publication_info=publication_info
        )

        search_query = SearchQuery(text)
        search_rank = SearchRank(search_vectors, search_query)

        return self.get_queryset().annotate(
            search=search_vectors
        ).filter(
            search=search_query
        ).annotate(
            rank=search_rank
        ).order_by('-rank')
