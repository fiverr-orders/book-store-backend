# Generated by Django 3.0.5 on 2020-04-30 13:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0005_auto_20200430_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookdetail',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='pages', to='books.Book'),
        ),
    ]
