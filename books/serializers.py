from rest_framework import serializers, fields

from books.models import Book, BookDetail


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class BookDetailSerializer(serializers.ModelSerializer):
    book = BookSerializer(read_only=True)
    total_pages = fields.SerializerMethodField('get_total_pages')

    class Meta:
        model = BookDetail
        fields = '__all__'

    def get_total_pages(self, instance):
        return instance.book.number_of_pages


class BookChapterSerializer(serializers.Serializer):
    book_id = fields.SerializerMethodField(read_only=True)
    chapter_title = fields.CharField(read_only=True)
    page_number = fields.IntegerField(read_only=True)

    class Meta:
        model = BookDetail
        fields = ('book_id', 'page_number', 'chapter_title',)

    def get_book_id(self, instance):
        return instance.book.id
