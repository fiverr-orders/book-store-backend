import json

from rest_framework import filters

from books.models import Book


class BookSearchFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        search_term = request.query_params.get('search_term', None)
        source = json.loads(request.query_params.get('search_source', '0'))
        author = json.loads(request.query_params.get('search_author', '0'))
        publisher = json.loads(request.query_params.get('search_publisher', '0'))
        published_date = json.loads(request.query_params.get('search_published_date', '0'))
        published_location = json.loads(request.query_params.get('search_published_location', '0'))
        publication_info = json.loads(request.query_params.get('search_publication_info', '0'))

        if search_term:
            return Book.objects.search(
                text=search_term,
                source=source, author=author, publisher=publisher, published_date=published_date,
                published_location=published_location, publication_info=publication_info
            )

        return queryset
