from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.tmp_storages import CacheStorage

from books.models import Book, BookDetail


class BookResource(resources.ModelResource):
    class Meta:
        model = Book


@admin.register(Book)
class BookAdmin(ImportExportModelAdmin):
    tmp_storage_class = CacheStorage
    resource_class = BookResource


class BookDetailResource(resources.ModelResource):
    class Meta:
        model = BookDetail


@admin.register(BookDetail)
class BookDetailAdmin(ImportExportModelAdmin):
    tmp_storage_class = CacheStorage
    resource_class = BookDetailResource
